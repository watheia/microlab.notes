---
id: e952ec28-5f46-46a9-9d6b-d2c14d3163a0
title: '2021-05-01'
desc: ''
updated: 1619905382250
created: 1619904562465
---

## Goals

1. [x] Setup Production Cluster
2. [x] Configure Gitlab Cluster Integration
3. [ ] Deploy compendium service

## Notes

### Project Sunflower Kickoff

Added the following new modules:

1. [watheia.sunflower.project](https://gitlab.com/watheia/sunflower/Compendium)
2. [watheia.sunflower.compendium](https://gitlab.com/watheia/sunflower/Compendium)
3. [watheia.sunflower.modelserver](https://gitlab.com/watheia/watheia.sunflower/modelserver)
4. [watheia.sunflower.cluster-management](https://gitlab.com/watheia/sunflower/cluster-management)
   _note:_ This was previously working in Ci/CD but got disconnected after secrets were reset. The installed artifact still works fine, so I will come back to this later.


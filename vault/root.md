---
id: af55c5e2-c6db-4b7d-a515-a55d13d9f9de
title: Home
desc: ''
updated: 1611350453794
created: 1611350453795
---
## Project Sunflower

> Launch the `MFE` capability for Watheia Labs IT consulting group.

### Resources

- [Charter](https://sunflower-project.vercel.app/)
- [Repo](https://gitlab.com/watheia/sunflower)
